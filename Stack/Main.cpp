#include <string>
#include "Evaluator.h"
#include <iostream>
using namespace std;
int main() {
	string expression;
	cout << "Enter expression:";
	cin >> expression;
	cout << endl;
	if (!Evaluator::allowed(expression)) {
		cout << "Not Allowed";
		return -1;
	}
	if (!Evaluator::checkBrackets(expression)) {
		cout << "Brackets broken";
		return -1;
	}
	string postfix = Evaluator::InfixToPostfix(expression);
	cout <<"Postfix: "<< postfix<<endl;
	cout<<Evaluator::evaluatePostfix(postfix);

}