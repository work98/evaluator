#pragma once
#include<string>
#include"Stack.h"
using namespace std;
static class Evaluator {
private:
	//���������� ��������� ��������.
	static int operatorWeight(char op)
	{
		switch (op)
		{
			case '+':
			case '-':
				return 1;

			case '*':
			case '/':
				return 2;

			case '(':
			case ')':
				return 5;
		}
	}
	//��������� ����������� ��������
	static bool OperatorPriority(char op1, char op2)
	{
		int op1Weight = operatorWeight(op1);
		int op2Weight = operatorWeight(op2);

		return op1Weight > op2Weight ? true : false;
	}
public:
	//�������� �� ���������� ��������
	static bool allowed(string expression) {
		for (int i = 0; i < expression.length(); i++) {
			if (!IsOperand(expression[i]) && !IsOperator(expression[i]))
				return false;
		}
		return true;
	}
	//�������� �� ���������� �������
	static bool checkBrackets(string expression) {
		int br = 0;
		for (int i = 0; i < expression.length(); i++) {
			if (expression[i] == '(') {
				br++;
			}
			else if (expression[i] == ')') {
				br--;
				if (br < 0) return false;
			}
		}
		return br == 0;
	}
	//�������� �� �������
	static bool IsOperand(char C)
	{
		if (C >= '0' && C <= '9') return true;
		return false;
	}
	//�������� �� ��������
	static bool IsOperator(char C)
	{
		if (C == '+' || C == '-' || C == '*' || C == '/' || C == '(' || C == ')')
			return true;
		return false;
	}
	//������� �� ��������� ����� ��������� � �����������
	static string InfixToPostfix(string expression)
	{
		Stack<char> S = Stack<char>();
		string postfix = "";
		for (int i = 0; i < expression.length(); i++) {

			if (expression[i] == ' ' || expression[i] == ',') 
				continue;
			else if (IsOperator(expression[i]))
			{
				if (expression[i] == '(')
				{
					S.push(expression[i]);
				}
				else if (expression[i] == ')')
				{
					while (!S.isEmpty() && S.top() != '(') {
						postfix += S.top();
						S.pop();
					}
					S.pop();
				}
				else {
					while (!S.isEmpty() && S.top() != '(' && OperatorPriority(S.top(), expression[i]))
					{
						postfix += S.top();
						S.pop();
					}
					S.push(expression[i]);
				}
			}
			else if (IsOperand(expression[i]))
			{
				
				postfix += expression[i];
			}

		}
		while (!S.isEmpty()) {
			postfix += S.top();
			S.pop();
		}
		return postfix;
	}
	//��������� ����������� ���������
	static int evaluatePostfix(string exp)
	{
		Stack<char> stack = Stack<char>();
		for (int i = 0; i<exp.length(); i++)
			stack.push(exp[i]);

		for (int i = 0; i<exp.length(); i++)
		{
			if (IsOperand(exp[i]))
				stack.push(exp[i] - '0');
			else if (IsOperator(exp[i]))
			{
				int val1 = stack.pop();
				int val2 = stack.pop();
				switch (exp[i])
				{
				case '+': 
					stack.push(val2 + val1);
					break;
				case '-': 
					stack.push(val2 - val1);
					break;
				case '*': 
					stack.push(val2 * val1);
					break;
				case '/': 
					stack.push(val2 / val1);
					break;
				}
			}
		}
		return stack.pop();
	}
};