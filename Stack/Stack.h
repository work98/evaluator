#pragma once
#include<iostream>
#include<stdio.h>

template<typename T>
class Stack {
private:
	T* vals;
	int size;
	int head;
public:
	//������� ����� ���� � �������� size.
	Stack(int size=1000) {
		vals = new T[size];
		head = -1;
		this->size = size;
	}
	//�������� ����� �� �������, ���������� true ���� �� ����, ����� false.
	bool isEmpty() {
		return head < 0;
	}
	//�������� ����� �� ������������, ���������� true ���� �� �����, ����� false.
	bool isFull() {
		return head == size-1;
	}
	//��������� ����� �������� value � ���� �����.
	void push(T value) {
		head++;
		vals[head] = value;
	}
	//���������� �������� � ������� �����, � �������� ������ �� 1 ����.
	T pop() {
		head--;
		return vals[head+1];
	}
	//���������� �������� � ������� �����, �� ������� ������.
	T top() {
		return vals[head];
	}
};


